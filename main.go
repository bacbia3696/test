package main

import "fmt"

func t() {
	fmt.Println("call t")
}

// Add comment
func t2() {
	fmt.Println("call t")
}

func main() {
	fmt.Println("add feature 1")
	fmt.Println("add feature 2")
	fmt.Println("add feature 3")
	fmt.Println("add feature 3")
	fmt.Println("Hello world")
}
