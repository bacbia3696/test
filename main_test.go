package main

import (
	"fmt"
	"testing"
)

func Test_main(t *testing.T) {
	t.Run("test 1", func(t *testing.T) {
		fmt.Println("Done test 1")
	})
	t.Run("test 2", func(t *testing.T) {
		fmt.Println("Done test 2")
	})
	t.Run("test 3", func(t *testing.T) {
		fmt.Println("Done test 3")
	})
	t.Run("test 4", func(t *testing.T) {
		main()
	})
	t.Run("test 5", func(t *testing.T) {
	})
}
